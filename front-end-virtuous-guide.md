# Back-end notes #

# Front-end notes #

## Components ##

### BaseController ###

Include it via DI and extend component capabilities via 

```javascript
    var vm = this;
    angular.extend(vm, BaseController);
```

BaseController contains the following features:

- translate
- hasRight
- cancel
- defaultFor
- getMessageFromResponse

These functions are copy-pasted to every single .js file and they are always the same. Injecting in where are needed is a better approach. An even better approach could be build Portal Utilities as global libraries (but more complex). Nice documentation can be found in [here](https://medium.freecodecamp.org/javascript-modules-a-beginner-s-guide-783f7d7a5fcc).

For functions which haven't state it works as charm. However response handling (getMessageFromResponse) it's slightly different and must use a dirtyier trick:

```javascript
/**
 * from $http response, handle the messages Portal way.
 * @param {any} response $http response
 */
_this.getMessageFromResponse = function (response) {
    if (response.status == 200) {
        this.communicationError = undefined;
        this.operationError = undefined;
        this.operationErrorCode = undefined;
    } else if (response.status == 0) {
        this.communicationError = response.statusText;
        this.operationError = undefined;
        this.operationErrorCode = undefined;
    } else {
        this.communicationError = undefined;
        this.operationError = response.statusText;
        this.operationErrorCode = response.status;
    }
};
```

`this` refer to caller.

E.g.: Any tedious `Promise` error handling could be written as: 

```javascript
notificationsService.getNotificationsFilterGroups()
    .then(
        function (response) {
            vm.getMessageFromReponse(response);
            vm.primaryFilterGroups = response.data;
            vm.togglePrimaryFilterGroupSelection(id);
            updateSecondaryFilterGroups();
        },
        function (response) {
            vm.getMessageFromReponse(response);
            vm.primaryFilterGroups = undefined;
            vm.togglePrimaryFilterGroupSelection(undefined);
        }
    );
```

instead of: 

```javascript
notificationsService.getNotificationsFilterGroups()
    .then(
        function (response) {
            vm.communicationError = undefined;
            vm.operationErrorCode = undefined;
            vm.operationError = undefined;

            vm.primaryFilterGroups = response.data;
            vm.togglePrimaryFilterGroupSelection(id);

            updateSecondaryFilterGroups();
        },
        function (response) {
            if (response.status == 0) {
                vm.communicationError = response.statusText;
                vm.operationErrorCode = undefined;
                vm.operationError = undefined;
            } else {
                vm.communicationError = undefined;
                vm.operationErrorCode = response.status;
                vm.operationError = response.statusText;
            }

            vm.primaryFilterGroups = undefined;
            vm.togglePrimaryFilterGroupSelection(undefined);
        }
    );
```

translations can be done just with `vm.translate("foo")`, authorization with `vm.hasRights("bar")`, and so on.

### Pagination ###

remove from every controller

```javascript
// Paginazione
vm.pageNumber = 0;
vm.pageSize = 15;
vm.pageSizes = [15, 30, 60];
vm.sortedColumn = undefined;
vm.sortedColumnAscending = true;


vm.firstPage = function () {
    vm.pageNumber = 1;

    updatePagedItems();
};

vm.previousPage = function () {
    vm.pageNumber = vm.pagedNotifications.PageNumber - 1;


    updatePagedItems();
};

vm.nextPage = function () {
    vm.pageNumber = vm.pagedNotifications.PageNumber + 1;

    updatePagedItems();
};

vm.lastPage = function () {
    vm.pageNumber = vm.pagedNotifications.TotalPages;

    updatePagedItems();
};

vm.selectPageSize = function (item) {
    vm.pageSize = item;
};

vm.$watch("pageSize", function (old, now) {
    if (old == now) {
        return;
    }

    updatePagedItems();
});
```

in controller, `updatePaged***` custom method should contain:

```javascript

// Note 1: refresh if needed
 /**
  * Call request for update to Pagination component.
  */
 function requestForItemsUpdate() {
    $scope.$broadcast("requestForUpdate");
 }
 vm.requestForItemsUpdate = requestForItemsUpdate;

// Note 2: if controller cannot know page, try reset pagination
 /**
  * Reset Pagination component
  */
 function resetPage() {
    $scope.$broadcast("resetPage");
 }

function updatePagedNotifications(pageNumber, pageSize) {
// Note 3: $location.search() try to call actual filters not relying on internal controller state which may be unsynchronized
    var filteringOptions = {
        "PrimaryFilterGroupId": $location.search().primaryFilterGroupId | 0,
        "SecondaryFilterGroupId": $location.search().secondaryFilterGroupId | 0,
        "SearchFilter": vm.searchFilter,
    };
    var pagingOptions = {
        "PageNumber": pageNumber,
        "PageSize": pageSize,
        "SortMember": vm.sortedColumn,
        "Ascending": vm.sortedColumnAscending,
        "SelectedItem": (vm.pageNumber == 0) ? $location.search().id : undefined,
    };
    notificationsService.getPagedNotifications(filteringOptions, pagingOptions)
        .then(
            function (response) {
// Note 4: handling response is always the same (not really fully implemented yet), so it relies on BaseController function which mirror ActualController variables.
                vm.getMessageFromResponse(response);
                var pagedItems = response.data;

// Note 5: pageNumber handling is not trivial: initially is handled by component, then is synchronized by backend due to its funcionalities (e.g.: handling id auto research)
                $scope.$broadcast("forcePageNumber", { pageNumber: response.data.PageNumber });
                vm.pagedNotifications = pagedItems;
                vm.pageNumber = vm.pagedNotifications.PageNumber;
                vm.toggleNotificationSelection($location.search().id);
            },
            function (response) {
                vm.getMessageFromResponse(response);
                vm.pagedNotifications = [];
                vm.toggleNotificationSelection(undefined);
            }
        );
}
```

Note 1, 2 and 5 are added to usual `GetPagedItems` functions.

p.s.: handling promises in that way:

```javascript
fetch('foo')
  .then(
    res => {
      // handle response
    },
    err => {
      // handle error
    }
  )
```

instead of: 

```javascript
fetch('foo')
.then(res => {
  // handle response
})
.catch(error => {
  // handle error
})
```

could not catch errors rised up inside the resolve function. (to be investigated).

**BENEFITS:**

- Potentially same Pagination logic everywhere that might changed, upgraded, fixed with a single modification.
- divide et impera responsabilities, funcion developing shouldn't worry about tech questions (e.g.: pagination)

**USAGE:**

```html
<pagination on-update="vm.updatePagedItems(pageNumber, pageSize)"
            total-pages="vm.pagedNotifications.TotalPages"
            default-page-size="15" />
```

p.s.: is pagination component to fire first list update when ready.

### Filtering ###

Filtering component aim to delegate filters responsability to an external controller instead of copy-paste its behaviour towards all funcionalities.

**USAGE**:

```html
<filtering icons-prefix="app/components/notifications/resources/sup_Notifications_"
           get-filter-groups="vm.getFilterGroup(parentGroup)"
           handle-response-errors="vm.getMessageFromResponse(response)"
           request-for-items-update="vm.requestForItemsUpdate()" />
```

It needs to know:

- where to retrieve icons
- specific filter calls (mirrored by Controllers) e.g.: 
    - vm.getFilterGroup = notificationsService.getNotificationsFilterGroups;
- response handler (mirrored by Controllers)
- how to request Items update (which should call pagination component refresh).

remove from controllers:

- update*FilterGroups
- toggle*FilterGroups
- $on($locationChanges)
- update

That component does:

1. retrieve Primary and Secondary filters.
2. every $locationChanges checks to Primary and Secondary changes and call requestForItemsUpdate every change. (that's the only way to handle Primary Filters since they are "owned" by NavigationController, though)
3. ensure requestForItemsUpdate to be called ONLY in the same navigation page (due to $locationChanges which fire on page leaving as well)

## == in Javascript ##

I suggest to avoid == using. Here's reasons:

```javascript
== Operator:
--------------------------------------
'' == '0': false
0 == '': true
0 == '0': true
false == 'false': false
false == '0': true
false == undefined: false
false == null: false
null == undefined: true
'\t\r\n' == 0: true


=== Operator:
--------------------------------------
'' === '0': false
0 === '': false
0 === '0': false
false === 'false': false
false === '0': false
false === undefined: false
false === null: false
null === undefined: false
'\t\r\n' === 0: false
```

consider also `falsy values` to compare reference values:

```javascript
false
null
undefined
0
NaN
''
""
```

e.g.: `if(vm.fooObj)` pass if object isn't in the list above (avoid: `if(vm.fooObj === null && vm.fooObj === undefined`).

BTW `null` is quite almost never used in Portal, because in JS a value is null if setted so.

## var clojures ##

JavaScript has `dynamic scope` in contrast with more easy `lexical scope`. E.g.:

```javascript
function updatePagedArticles() {
    var id = $location.search().componentSelectedId;
    var pagingOptions = {
        ...
        "SelectedItem": ($scope.pageNumber == 0) ? id : undefined
    };

    articlesService.getPagedArticles(filteringOptions, pagingOptions)
        .then(
        function (response) {
            ...
            $scope.toggleArticleSelection(id);
        });
}
```

Is possible in JavaScript, but not in C#, because `id` is a `updatePagedArticles` variable, while is used also in the `response anonymous` function. Avoid this feature because it reproduce unintended behaviours (unless you really mean it). 

In that specific case `id` variable is binded to outer function value, while `location.search().componentSelectedId` might has changed. (questo ha causato il bug del flickering della selezione nelle liste paginate).

The general rule is always use the smaller scope as possible. Although is really easy to access global variables (expecially in JS), itsn't necessary and it's very often dangerous.

### $scope vs. this ###

In portal application, everything is binded to `$scope`. Although that doesn't really mean errors (because we usually have 1 controller per page), that's not really a good practice mainly for two reasons:

1. $scope polluting. JavaScript doesn't check naming overlap, so variables or functions might be overloaded silently
2. the pattern is copied everywhere and even variables which shouldn't be in $scope, are there. Overhead and polluting.

Just bind variables to closer scope you can (se above paragraph): 

```javascript
function privateFunction() {}

function publicFunction() {}
$scope.publicFunction = publicFunction;
```

A mainly used approach would be controllerAs syntax (mandatory in AngularJS 4):

```javascript
var _this = this;

function privateFunction() {}

function publicFunction() {}
_this.publicFunction = publicFunction;
```

```html
<div ng-controller"fooController As vm">
    <button ng-click="vm.publicFunction()"></button>
</div>
```

However this require very spread changes towards every controller in Portal application, so I don't reccomend change it (but for new features).

## Team tools ##

The following paragraph is specifically written with VSCode in mind (just because I use it), but it can be easily applyied to Visual Studio or Web Storm IDEs.

Modern tools use text-driven configuration for:
    
- task as build, run, etc. configuration
- code formatting
- code practice
- external tools installation, configuration and usage

**WHY text-driven configuration is cool:** can be statically declared and shared through collegues. So, for instance, no more weird code merge because everyone use the same code formatting (e.g.: tabs as 3 space once for all), provide out of the box developing tools (new collegues can have same build, run task, web-server tools just copying text-diven configuration), and so on.

Furthermore just copying text-configuration files through workspaces, it's fast to replicate workspaces (as for DEV, Main, Gres1 workspaces).

In our's backend developing chain is basically provided by Visual Studio but some exceptions. This is still not valid for front-end developing.

However these features could be extended as needed for:

- Testing tasks
- More complex build tasks

### Run task ###

Text-configuration for having an external chrome instance attached to a debug session:

(.vscode\launch.json) install chrome debugger extension and put it in project root folder (same level as app/)
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Launch localhost",
            "type": "chrome",
            "request": "launch",
            "userDataDir": "/temp/chrome-debug",
            "url": "http://localhost:8000",
            "webRoot": "${workspaceRoot}"
        },
        {
            "type": "chrome",
            "request": "attach",
            "name": "Attach to chrome",
            "port": 9222,
            "webRoot": "${workspaceRoot}"
        }
    ]
}
```

text-configuration for having a web-server up and running rely on npm (`package.json` definitions):

```cmd
C:\tfs\DAP2014\4000096374_PSTLG1\Interfaces\Portal - New\src>npm start
```

(live-server, must be installed first: see Appendix A).

Live-server is a simple server which auto-reload web-sites files at every change.

### Intellisense ###

VSCode already ships with TypeScript. Running `npm install` as specified in `Appendix A`, it will be installed Intellisense for:

- AngularJS
- kendo-ui
- jquery

Could be used for eventually additional vendor packages or even for Portal Types if specified.

### Linter ###

Install ESLint extension or just have a Lint server running.

Code conventions might be taken from bigger companies and extended:
```json
module.exports = {
    "env": {
        "browser": true
    },
    "extends": ["google"], // "plugin:angular/johnpapa"], //
    "rules": {
        "linebreak-style": [
            "error",
            "windows"
        ],
        "space-before-function-paren": [
            "error",
            {
                "anonymous": "always",
                "named": "never",
            }
        ],
        "padded-blocks": ["off"],
        "no-var": ["off"],
        "object-curly-spacing": ["off"],
        "no-invalid-this": ["off"],
        "max-len": [
            "error",
            {
                "code": 120,
            }
        ],
        "eol-last": ["off"],
        "quotes": [
            2,
            "double",
            {
                "allowTemplateLiterals": true
            }
        ],
        // angular-plugin
        // "angular/file-name": [0],
        // "angular/controller-name": [2, "/^[a-z][a-zA-Z0-9]*Controller/"],
    }
};
```

That't a very used tool. For more infos: http://eslint.org/docs/user-guide/configuring

### Code Formatting ###

VSCode ships with text-configurable formatting (see conf with `ctrl + ,`: 

Default `settings.json`
```json
...
  "editor.tabSize": 4,
...
```
Custom `settings.json`
```json
...
    "editor.detectIndentation": false,
    "html.format.wrapAttributes": "force-aligned",
...
```


## Appendix A ##

Every JS tool can be installed as follows:

1. Copy paste `package.json`, which declare every tool dependencies:

```json
{
  "name": "portal",
  "version": "1.0.0",
  "description": "",
  "scripts": {
      "start": "node node_modules\\live-server\\live-server.js --no-browser --port=8000 --ignore=.\\tests\\**,.\\node_modules\\**,.\\.vscode ..\\src",
      "smoke": "START /D tests\\robot _run-tests.bat",
      "test": "node node_modules\\karma\\bin\\karma start karma.conf.js",
      "test-debug": "node node_modules\\karma\\bin\\karma start karma.conf.js --browsers Chrome_debug",
      "test-load": "node node_modules\\artillery\\bin\\artillery run tests\\load\\orders.yml -o orders-results.yml && node node_modules\\artillery\\bin\\artillery report orders-results.yml"
   },
  "author": "",
  "devDependencies": {
    "@types/angular": "^1.5.8",
    "@types/kendo-ui": "^2017.1.7",
    "@types/jquery": "^3.2.8",
    "eslint": "latest",
    "eslint-config-google": "latest",
    "eslint-plugin-angular": "^3.0.0",
    "live-server": "^1.2.0",
    "typescript": "latest"
  },
  "dependencies": {
  }
}
```

2. run `npm install` in workspace folder. That will build tools in `node_modules` folder. They will be used transparently by VSCode (or whatever). (If you don't have npm, just download it https://nodejs.org/it/download/)
3. `@types` folder contains JS type definitions just for intellisense. We actually freeze angular and front end production libraries in repo (angular, bootstrap, jquery, moment, kendo), inside `vendor` folder